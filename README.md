Dies ist eine Modifizierte Version von dem <b>Laut.FM Sendeplan welcher durch Fabians-Welt</b> erstellt und zur Vef&uuml;gung gestellt wurde.

Bei diesem Sendeplan muss nur der <b><i>Stationsname</i></b> ausgetauscht werden und bei bedarf die <b><i>Stylesheetdatei</i></b> angepasst werden.

Die Stylesheet-Datei kann wie folgt eingebunden werden 

&lt;link rel="stylesheet" href="sendeplan.css"&gt;

Nat&uuml;rlich muss der Dateipfad angepasst werden je nach dem wo die Stylesheetdatei liegt.